ESTE PROGRAMA FAZ USO DOS DICIONÁRIOS WORDNET ABAIXO:

CITAÇÕES E REFERENCIAS:

NLPNET

    Fonseca, E. R. and Rosa, J.L.G. A Two-Step Convolutional Neural Network Approach for Semantic Role Labeling. Proceedings of the 2013 International Joint Conference on Neural Networks, 2013. p. 2955-2961 [PDF]

    Fonseca, E. R. and Rosa, J.L.G. Mac-Morpho Revisited: Towards Robust Part-of-Speech Tagging. Proceedings of the 9th Brazilian Symposium in Information and Human Language Technology, 2013. p. 98-107 [PDF]

http://nilc.icmc.sc.usp.br/nlpnet/
http://nilc.icmc.sc.usp.br/nlpnet/models.html#pos-portuguese



*OPEN WORDNET-PT
http://compling.hss.ntu.edu.sg/omw/
https://github.com/own-pt/openWordnet-PT

Referencias:
Valeria de Paiva and Alexandre Rademaker (2012)
Revisiting a Brazilian wordnet. In Proceedings of Global Wordnet Conference, Matsue. Global Wordnet Association. (also with Gerard de Melo's contribution) 
    
* WORDNET
Referencias: 
George A. Miller (1995). WordNet: A Lexical Database for English.
Communications of the ACM Vol. 38, No. 11: 39-41.
Princeton University "About WordNet." WordNet. Princeton University. 2010. 
Christiane Fellbaum (1998, ed.) WordNet: An Electronic Lexical Database. Cambridge, MA: MIT Press.

Disponível em :
https://wordnet.princeton.edu/

PRÓXIMOS PASSOS - FRONT-END:
https://docs.djangoproject.com/en/2.0/intro/
https://www.tutorialspoint.com/django/django_file_uploading.htm

ANÁLISE DE SIMILARIDADE : 
Corrigir problemas com cálculo das métricas (cosine-similarity / manhatan / euclidian)
http://scikit-learn.sourceforge.net/dev/auto_examples/index.html
