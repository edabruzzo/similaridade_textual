
****************************************************
DOCUMENTA��O DAS VERS�ES DOS PACOTES UTILIZADOS
Vers�o do Python 3.6.5 (v3.6.5:f59c0932b4, Mar 28 2018, 17:00:18) [MSC v.1900 64 bit (AMD64)]
Vers�o do Pandas 0.23.3
Vers�o do NLTK   3.3
Autor do NLTK   Steven Bird, Edward Loper, Ewan Klein
Vers�o do PyPDF  1.26.0
Autor do PyPDF  Mathieu Fenniak
Vers�o do BeautifulSoup  4.6.0
Autor do BeautifulSoup  Leonard Richardson (leonardr@segfault.org)
Vers�o do Urllib3  1.23
Autor do UrlLib3  Andrey Petrov (andrey.petrov@shazow.net)
Vers�o do SciKit Learn 0.19.2
Vers�o do Matplotlib   2.2.2
Vers�o do Seaborn   0.9.0
****************************************************