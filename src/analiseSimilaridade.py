
#coding:UTF-8
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
import numpy as np
import nltk
from nltk.corpus import wordnet as wn
import pandas as pd
from langdetect import detect
from string import punctuation
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.metrics.pairwise import cosine_similarity
from scipy import linalg, mat, dot
import os
import string
import random
    
#https://textminingonline.com/dive-into-nltk-part-vii-a-preliminary-study-on-text-classification
#https://pythonprogramming.net/words-as-features-nltk-tutorial/
#https://www.ibm.com/developerworks/br/library/os-pythonnltk/index.html
def treinarClassificadorNLTK(dicionarioPositivo, dicionarioNegativo):
    
    listaPositiva = []
    listaNegativa = []
    palavrasPositivas = {}  
    
    for conteudo in dicionarioPositivo.values():
        palavrasPositivas = nltk.FreqDist(processaDados(conteudo))
        
        print(palavrasPositivas)
    '''
    for conteudo in dicionarioPositivo.values():
        listaPositiva.append(processaDados(conteudo))
    for conteudo in dicionarioNegativo.values():
        listaNegativa.append(processaDados(conteudo))
    '''
    features_positivas = dict((palavra, True) for palavra in listaPositiva)
    #features_positivas = list(map(dic_features_positivas, listaPositiva))
    
    print(features_positivas)
    
    features_negativas = dict((palavra, False) for palavra in listaNegativa)
    
    #features_negativas = list(map(dic_features_negativas, listaNegativa))
    
    features = features_positivas + features_negativas
    
    random.shuffle(features)
    #80% para treinamento
    #Necessário fazer um cast para integer
    conjuntoTreinamento = features[:(int(len(features)* 0.8))]
    #20% para teste
    conjuntoTeste = features[:(int(len(features)* 0.2))]
    
    print('Tamanho do conjunto total de features: {}'.format(len(features)))
    print('Tamanho do conjunto de treinamento: {}'.format(len(conjuntoTreinamento)))
    print(conjuntoTreinamento)    
    print('Tamanho do conjunto de teste: {}'.format(len(conjuntoTeste)))
    print(conjuntoTeste)
    classificador = nltk.NaiveBayesClassifier.train(conjuntoTreinamento)
    print("Percentual de acurácia do classificador:",(nltk.classify.accuracy(classificador, conjuntoTeste))*100)

def geraSynsets(conteudo):
    
    if (conteudo is not None):
        lingua_stop = 'portuguese'
        lingua_doc = 'por'
        try:
            lingua_doc = detect(conteudo)
        except:
            pass
        print('Língua do documento: ')
        print(lingua_doc)
        if('pt' in lingua_doc): 
            lingua_doc = 'por'
        if('en' in lingua_doc): 
            lingua_doc = 'eng'
        if('pt' in lingua_doc):
            lingua_stop = 'portuguese'
        if('en' in lingua_doc):
            lingua_stop = 'english'
            
        stopwords = nltk.corpus.stopwords.words(lingua_stop) + list(punctuation)
        tokens = nltk.tokenize.word_tokenize(conteudo)
        tokensSemStopWords = [token.lower() for token in tokens if token not in stopwords and len(token)>5]
        quantidadeTokens = len(tokensSemStopWords)
        listaSynsets = []
    
        for token in tokensSemStopWords:
            synsets = wn.synsets(token, lang=lingua_doc)
            if len(synsets) > 0 :
                listaSynsets.append(synsets[0])
        print('Lista Synsets do documento')
        print(listaSynsets)        
        return listaSynsets, quantidadeTokens              
    else:
        pass     

def calcula_similaridade(synsets1, synsets2):
    similaridade=[]
    for synset1 in synsets1:
        #print(synset1)
        valorSimilaridade = [valor for valor in [synset1.path_similarity(synset2) for synset2 in synsets2]if valor is not None]
        if valorSimilaridade:
           #print(valorSimilaridade)
           similaridade.append(max(valorSimilaridade))
    valorTotalSimilaridade = sum(similaridade)/len(similaridade)
    return valorTotalSimilaridade

def testaSimilaridadeSynSets(conteudoSuspeito, conteudoFonte):
    
    #processo = os.getpid(   )
    if(conteudoFonte is not None):
        #doc1 = leituraPDF('C:/Users/Dell E5410/Documents/ANALISE_E_DESENVOLVIMENTO_DE_SISTEMAS.pdf')
        #doc2 = leituraPDF('C:/Users/Dell E5410/Documents/ANALISE_E_DESENVOLVIMENTO_DE_SISTEMAS.pdf')
        synsets1, quantidadeTokens1 = geraSynsets(conteudoSuspeito)
        synsets2, quantidadeTokens2 = geraSynsets(conteudoFonte)
        calculo1 = calcula_similaridade(synsets1, synsets2)
        calculo2 = calcula_similaridade(synsets2, synsets1)
        '''
        calculo1 = Parallel(n_jobs=4, verbose=1, backend='multiprocessing')(
                            delayed(calcula_similaridade)(synsets1, synsets2))
        calculo2 = Parallel(n_jobs=4, verbose=1, backend='loky')(
                            delayed(calcula_similaridade)(synsets2, synsets1))
        
        '''
        resultadoSimilaridade = ((calculo1 + calculo2)/2) * 100
        quantidadeTotalTokens = 0
        quantidadeTotalTokens = quantidadeTokens1 + quantidadeTokens2
        print('Valor Final de Similaridade em %')
        print(resultadoSimilaridade)
        return resultadoSimilaridade, quantidadeTotalTokens
    else: 
        pass

def processaDados(conteudo):
    
    tokens = nltk.tokenize.word_tokenize(conteudo)
    stopWords = set(nltk.corpus.stopwords.words('portuguese'))
    pontuacao = set(string.punctuation)
    tokensSemStopWords = [token.lower() for token in tokens if (token not in stopWords and token not in pontuacao and len(token)>4)]
    #porter_stemmer = nltk.stem.PorterStemmer()    
    #portuguese_stemmer = nltk.stem.RSLPStemmer() #http://www.nltk.org/howto/portuguese_en.html
    #Escolher qual stemmer utilizar
    #stemmer = portuguese_stemmer
    #conteudoProcessado = [stemmer.stem(token) for token in tokensSemStopWords]
    conteudoProcessado = [token for token in tokensSemStopWords]
    return conteudoProcessado

#https://github.com/MaxPoon/coursera-Applied-Data-Science-with-Python/blob/master/Applied-Text-Mining-In-Python/week3/Assignment%2B3.ipynb    
def calculaSimilaridadeCosseno(dicionarioConteudo):

    print('VERIFICANDO SE O DICIONÁRIO ESTÁ PREENCHIDO')
    print(dicionarioConteudo)        
    #file = open(os.getcwd().replace('src','/documentos_testados/ResultadoAnalise.txt'), 'a')
    #tokenizador = nltk.data.load('tokenizers/punkt/portuguese.pickle')
    #sentencasSuspeitas = tokenizador.tokenize(documentoSuspeito)
    #sentencasFonte = tokenizador.tokenize(documentoFonte)
    stopWords = nltk.corpus.stopwords.words('portuguese')
    #vetorizador_tfidf = TfidfVectorizer(stop_words = stopWords, use_idf=True)
    vetorizador_tfidf = TfidfVectorizer(stop_words = stopWords, 
                                        tokenizer = processaDados
                                        #use_idf=True
                                        )
    try:
        matriz_tfidf = vetorizador_tfidf.fit_transform(dicionarioConteudo.values())
    except:
        print('Problema na geração da matriz tfidf')
        pass
    else:
        print('Dimensões da matriz tfidf')
        #file.write('Dimensões da matriz tfidf'+'\n')
        print(matriz_tfidf.shape)
        #file.write(matriz_tfidf.shape)
        #file.write('\n')
        print('MATRIZ TF-IDF'+'\n')
        print(matriz_tfidf)
        print('')
        #matriz_similaridade_cosseno = pairwise_distances(matriz_tfidf, metric="cosine", n_jobs=1)
        #matriz_similaridade_euclidiano = pairwise_distances(matriz_tfidf, metric="euclidean", n_jobs=1)
        similaridade_cosseno = cosine_similarity(matriz_tfidf[0], matriz_tfidf[1])
        #print('SIMILARIDADE COSSENO :'+ similaridade_cosseno)
        #file.write('SIMILARIDADE COSSENO :\n')
        print(similaridade_cosseno)
        #print(matriz_similaridade_cosseno)
        print('Percentual de similaridade: %.2f' %similaridade_cosseno*100)
        #file.write('Percentual de similaridade: {0:.2f} %'.format(similaridade_cosseno * 100))
        #file.write('\n')
        print('****************************\n')
        #file.write('****************************\n')
        #file.close()
        return similaridade_cosseno
    '''
   
    #print('Distância euclidiana')
    #print(matriz_similaridade_euclidiano)
    print('*****************************')
    valor_linha = 0
    for i in range(len(matriz_similaridade_cosseno)):
            #print(pairwise_distances(matriz_tfidf[i-1:i], matriz_tfidf, metric="precomputed", n_jobs=1))
            valor_linha +=  max(matriz_similaridade_cosseno[i])
    valorTotal = valor_linha/len(matriz_similaridade_cosseno) * 100
    print('Percentual de similaridade: %.2f' %valorTotal)
    '''
    
import matplotlib.pyplot as plt
#Referência: https://medium.com/@aneesha/visualising-top-features-in-linear-svm-with-scikit-learn-and-matplotlib-3454ab18a14d
def plotCoeficientes(classificador, features, top_features=20):
    coeficientes = classificador.coef_.ravel()
    coeficientes_top_positivos = np.argsort(coeficientes)[-top_features:]
    coeficientes_top_negativos = np.argsort(coeficientes)[:top_features]
    top_coeficientes = np.hstack([coeficientes_top_negativos, coeficientes_top_positivos])
    # create plot
    plt.figure(figsize=(15, 5))
    cores = ['red' if c < 0 else 'blue' for c in coeficientes[top_coeficientes]]
    plt.bar(np.arange(2 * top_features), coeficientes[top_coeficientes], color=cores)
    features = np.array(features)
    plt.xticks(np.arange(1, 1 + 2 * top_features), 
               features[top_coeficientes], 
               rotation=60, 
               ha='right')
    plt.show()    

from leituraDocumento import lerDocumento 
from sklearn.naive_bayes import MultinomialNB 
from time import time
from sklearn.feature_extraction.text import CountVectorizer
import pandas as pd 
 #Referencia: http://scikit-learn.org/stable/modules/naive_bayes.html
 #https://github.com/MaxPoon/coursera-Applied-Data-Science-with-Python/blob/master/Applied-Text-Mining-In-Python/week3/Assignment%2B3.ipynb
# http://scikit-learn.org/stable/auto_examples/linear_model/plot_sparse_logistic_regression_20newsgroups.html#sphx-glr-auto-examples-linear-model-plot-sparse-logistic-regression-20newsgroups-py
def treinarClassificador_SciKitLearn(dicionarioPositivo, dicionarioNegativo):
    
    conteudoDicionarioTreinamento = {}
    conteudoDicionarioTeste = {}
    listaTreinamento = []
    listaTeste = []
    
    listaTeste.append(listaArquivosBaixados.pop())
    listaTeste.append(listaArquivosBaixados.pop())
    listaTeste.append(listaArquivosBaixados.pop())
    listaTeste.append(listaArquivosBaixados.pop())
    listaTeste.append(listaArquivosBaixados.pop())
    listaTeste.append(listaArquivosBaixados.pop())
    listaTeste.append(listaArquivosBaixados.pop())
    listaTeste.append(listaArquivosBaixados.pop())
    
    #TEXTO DE ANALISE DE SISTEMAS
    arquivoAnaliseSistemas = 'C:/Users/Dell E5410/Documents/ANALISE_E_DESENVOLVIMENTO_DE_SISTEMAS.pdf'
    #TEXTO DE FARMACOBOTÂNICA
    arquivoBotanica = 'C:/Users/Dell E5410/Documents/LAJOP_24_1_1_1_6WQ842B4X2.pdf'#FARMACOBOTÂNICA
    
    listaTeste.append(arquivoAnaliseSistemas)
    listaTeste.append(arquivoBotanica)
    
    
    print('Tamanho da lista de teste: '+str(len(listaTeste)))
    listaTreinamento = listaArquivosBaixados
    print('Tamanho da lista de treinamento: '+str(len(listaTreinamento)))
   
    for caminhoArquivo in listaTreinamento:
            conteudoArquivo = lerDocumento(caminhoArquivo)
            if(conteudoArquivo is not None):
                conteudoDicionarioTreinamento[caminhoArquivo] = conteudoArquivo
    
    for caminhoArquivo in listaTeste:
        conteudoArquivo = lerDocumento(caminhoArquivo)
        if(conteudoArquivo is not None):
            conteudoDicionarioTeste[caminhoArquivo] = conteudoArquivo
   
    stopWords = nltk.corpus.stopwords.words('portuguese') + list(string.punctuation)
    #vetorizador_tfidf = TfidfVectorizer(stop_words = stopWords, use_idf=True)
    print("Extraindo features dos dados de treinamento e teste usando um vetorizador Tfidf (matrizes esparsas)")
    tempo_antes_vetorizador = time()
    vetorizador = CountVectorizer().fit(conteudoDicionarioTreinamento.values())
    distribFrequencia = nltk.FreqDist(vetorizador.get_feature_names())
    distribFrequencia.tabulate()
    distribFrequencia.plot(100, cumulative=True)
    distribFrequencia.plot(100, cumulative=False)
    
    
    vetorizador_tfidf = TfidfVectorizer(stop_words = stopWords, 
                                        tokenizer = processaDados, 
                                        use_idf=True,
                                        #prevenção de divisão por zero
                                        smooth_idf=True).fit(conteudoDicionarioTreinamento)
    
    #matriz_tfidf_treinamento = vetorizador_tfidf.fit_transform(dicionarioConteudoTreinamento)
    matriz_tfidf_treinamento = vetorizador_tfidf.transform(conteudoDicionarioTreinamento)
    
    
    
    #http://scikit-learn.org/stable/auto_examples/text/document_classification_20newsgroups.html#sphx-glr-auto-examples-text-document-classification-20newsgroups-py
    #perceba-se que no teste foi aplicado apenas transform ao invés do fit_transform
    matriz_tfidf_teste = vetorizador_tfidf.transform(conteudoDicionarioTeste.values())
    
    tempo_apos_vetorizador = time() - tempo_antes_vetorizador
    print("Tempo de vetorização: %fs " % (tempo_apos_vetorizador))
    print('Dimensões da matriz tfidf de treinamento:')
    print(matriz_tfidf_treinamento.shape)
    print('Matriz tfidf de treinamento: ')
    print(matriz_tfidf_treinamento)
    print("Examplos de treinamento: n_samples: %d, n_features: %d" % matriz_tfidf_treinamento.shape)
    print('Matriz tfidf de teste: ')
    print(matriz_tfidf_teste)
    print("Examplos de teste: n_samples: %d, n_features: %d" % matriz_tfidf_teste.shape)
    
    model_Multinomial_Nayve_Bayes = MultinomialNB(alpha=0.1)
    #Bernoulli apenas suporta valores binários
    #model_Bernoulli_Nayve_Bayes = BernoulliNB(alpha=1.0, binarize=0.0, fit_prior=True, class_prior=None)
    #classe = [temaDocumentoSuspeito]
    #dados_treinamento = {'Documento vetorizado':[matriz_tfidf_treinamento],  
    y_treinamento = np.ones(len(conteudoDicionarioTreinamento), dtype=int)
#    y_teste = np.asarray[]
    #df = pd.DataFrame(data)
    model_Multinomial_Nayve_Bayes.fit(matriz_tfidf_treinamento, y_treinamento)
    predicao = model_Multinomial_Nayve_Bayes.predict(matriz_tfidf_teste)
    #print('Teste de predição de um texto da classe artigo científico não etiquetado')
    #if(predicao == 1):
    #    print('O texto testado é sobre Artigo científico')
    #else:
    #    print('O texto não possui relação com Artigo científico')
    #plotCoeficientes(model_Multinomial_Nayve_Bayes, features_treinamento)
    print(predicao)
    
        
