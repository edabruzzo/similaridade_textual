#coding:UTF-8
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
from heapq import nlargest
import nltk
from string import punctuation
from matplotlib import pylab



def processarTexto(conteudo):
    #ESTOU PARTINDO DO PRESSUPOSTO QUE OS TEXTOS SUBMETIDOS ESTARÃO EM PORTUGUES
    #PORÉM, PRECISAMOS PENSAR EM CENÁRIOS EM QUE O TEXTO FOI TRADUZIDO E PARAFRASEADO
    #DE UMA OUTRA LÍNGUA
    
    listaSentencas = []
    
    '''
    #SOMENTE USAR ESTA ABORDAGEM SE EU CONTINUAR TENDO ERRO DE PERMISSÃO
    # NO MOMENTO DE CARREGAR STOPWORDS PELO NLTK NA PASTA 
    # C:/Users/Dell%20E5410/AppData/Roaming/nltk_data/corpora/stopwords
    # Mudei o nome da pasta e vou forçar um novo download no tratamento do erro    
    
    stopWords = ''
    with open(os.getcwd().replace('src', 'stopwords/portuguese'))) as fileStopWord:
         for line in fileStopWord:
            stopWords += line
    '''
    try:
      
        #Esta instrução lança uma exceção LookupError se o pacote punkt do nltk não estiver instalado
        #De repente comecei a ter problema com permissão de acesso a pasta 
        #C:/Users/Dell%20E5410/AppData/Roaming/nltk_data/corpora/stopwords
        #acabei copiando a pasta e lendo o arquivo
        stopwords = nltk.corpus.stopwords.words('portuguese') + list(punctuation)        
    #Só faz o download do pacote punkt do nltk na primeira execução
    except LookupError:
        nltk.download('punkt')
        nltk.download('stopwords')
    else:
    
        print('STOPWORDS: ')
        print(stopwords)
        
        #print(conteudo)
        #http://www.nltk.org/howto/portuguese_en.html
        tokenizador_sentencas = nltk.data.load('tokenizers/punkt/portuguese.pickle')
        #sentencas = nltk.tokenize.sent_tokenize(conteudo)
        sentencas = tokenizador_sentencas.tokenize(conteudo)
        print(sentencas)
        palavras = nltk.tokenize.word_tokenize(conteudo)
        #print(palavras)
        
        #Dependendo do local em que estiver esta instrução, ela pega apenas letras e não palavras ou sentenças inteiras e não palavras
        palavrasSemStopWords = [palavra.lower() for palavra in palavras if palavra not in stopwords and len(palavra)> 5] 
        distrFreq = nltk.FreqDist(palavrasSemStopWords)
        distrFreq.tabulate()
        print('Distribuicao de Frequencia das palavras no texto, sem stopwords')

        for chave,valor in distrFreq.items(): 
           print('**************************')
           print ('Palavra: '+str(chave) + '- frequencia: ' + str(valor))
        distrFreq.plot(50, cumulative=True)
        distrFreq.plot(50, cumulative=False)
        
        sentencas_importantes = []
        
        #Trecho de código disponível em 
        # https://medium.com/@viniljf/utilizando-processamento-de-linguagem-natural-para-criar-um-sumariza%C3%A7%C3%A3o-autom%C3%A1tica-de-textos-775cb428c84e
        for sentenca in sentencas:
            for palavra in nltk.tokenize.word_tokenize(sentenca.lower()):
                if palavra in distrFreq and distrFreq[palavra]>10:
                    sentencas_importantes.append(sentenca)
        
        for sentenca in sentencas_importantes:
            print('Sentencas mais importantes : ')
            print(sentenca) 
        '''
        palavras_importantes = [palavra for palavra in palavrasSemStopWords 
                                if(distrFreq[palavra]>10)]
        '''
        return sentencas_importantes
        #return palavras_importantes

#def testarDocumentos(conteudoSuspeito, fonte):
    
    #https://pypi.org/project/Fuzzy/
    #http://stackabuse.com/levenshtein-distance-and-text-similarity-in-python/
    
    
