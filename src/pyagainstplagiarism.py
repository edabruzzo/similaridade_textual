﻿#encoding=UTF-8
#! C:/Users/Dell E5410/VIRTUALENVS/PyAgainstPlagiarism/VIRTUALENV/bin/python
 


# Referencias
# https://www.fullstackpython.com/api-integration.html
# https://www.tutorialspoint.com/python/python_strings_as_files.htm
#http://www.pitt.edu/~naraehan/python2/index.html
#https://fernandofreitasalves.com/tutorial-virtualenv-para-iniciantes-windows/ 
#https://virtualenv.pypa.io/en/latest/userguide/


#importa as bibliotecas que serao utilizadas no projeto
import os
import processamentoTexto as process
import webScraping  as web   
from leituraDocumento import lerDocumento    
import urllib
import analiseSimilaridade as analise
from time import time
import nltk
import pandas as pd
import matplotlib.pyplot as plt
import sys 
import matplotlib
import PyPDF2 #APENAS PARA DOCUMENTAR VERSÃO
import sklearn #APENAS PARA DOCUMENTAR VERSÃO
#import classificacao as classif
#Referencias: 
#https://code.tutsplus.com/tutorials/how-to-download-files-in-python--cms-30099
#https://www.py4e.com/html3/12-network
#urllib.request.urlretrieve funciona melhor que urllib.request.urlopen
# o urlretrieve já grava em disco o pdf lido da url.
# como o pdf lido será comparado com o arquivo suspeito, ele poderá ser eliminado após leitura
# de posse do conteúdo do documento remoto vou compará-lo com o conteúdo suspeito
listaArquivosBaixados = []

global temaDocumentoSuspeito
global conteudoDicionarioRemoto


def extraiConteudoRemoto(url):
   
    print('Extraindo conteúdo da seguinte url: '+url)
    #NÃO ESTÁ CONSEGUINDO LER O DOCUMENTO
    #url = 'http://kamloopsmassagematters.ca/wp-content/uploads/2016/07/chest-wall.pdf'
    #urlretrieve é perfeito, pois já grava o arquivo em disco
    # além disso, o urlopen não está funcionando corretamente
    #arquivoRemoto = urllib.request.urlopen(url)
    #tamanho = 0
    #conteudoDicionarioRemoto = {}
    nomeCaminhoArquivoRemoto = os.getcwd().replace('src', 'documentos_testados/')+ url.replace('/', '_').replace(':', '_')
    try:
        urllib.request.urlretrieve(url, nomeCaminhoArquivoRemoto)
    except:
        print("Ocorreu um erro - linha 46 - pyagainstplagiarism")
        pass
    print('Caminho do arquivo remoto extraído: '+ nomeCaminhoArquivoRemoto)
    listaArquivosBaixados.append(nomeCaminhoArquivoRemoto)
    conteudoFonte = lerDocumento(nomeCaminhoArquivoRemoto)
    #conteudoDicionarioRemoto[nomeCaminhoArquivoRemoto] = conteudoFonte
    try:
        print('Removendo arquivo remoto: {}'.format(nomeCaminhoArquivoRemoto))
        os.remove(nomeCaminhoArquivoRemoto)
    except:
        print('Erro na remoção do arquivo pdf no seguinte caminho: {}'.format(nomeCaminhoArquivoRemoto))
        pass
    #import requests
    #arquivoRemoto = requests.get(url)
    # with open(nomeCaminhoArquivoRemoto, 'wb') as documento:
     #       documento.write(arquivoRemoto.content)
    '''
    while True:
        dados = arquivoRemoto.read(100000)
        if len(dados)< 1 : break
        tamanho += len(dados)
        with open(nomeCaminhoArquivoRemoto, 'wb') as documento:
            documento.write(dados)
            #documento.write(arquivo.content)
    '''
    #conteudoFonte = lerDocumento(arquivoFonte)
    #print(conteudoFonte)
    print('Conteúdo extraído da seguinte fonte: '+url)
    print(conteudoFonte)
    return conteudoFonte

from datetime import datetime


def main():
    #treinarClassificador()
    efetuarAnaliseWebScraping()
    #plotarResultados()
    #web.testarTorRequest()
    
    
    
    
import seaborn as sns

def plotarResultados(df=None):
    #https://seaborn.pydata.org/tutorial.html
    #https://pandas.pydata.org/pandas-docs/stable/visualization.html
    nomeArquivo = 'ResultadoAnalise_DataFrame_2018-08-26 12-05-26.411450_.csv' 
    caminhoArquivo = os.getcwd().replace('src','documentos_testados/Resultados/'+nomeArquivo)
    df2 = pd.read_csv(caminhoArquivo)
    #x = df2['Tamanho arquivos analisados em Mb']
    x = pd.Series(range(0,len(df2['Url do documento analisado'])))
    #x = df2['Tamanho arquivos analisados em Mb']
    #y1 = df2['Tempo_analise_similaridade pelo Cosseno']
    #y2 = df2['Tempo_analise_similaridade por path similarity - synsets']
    #x = df2['']
    y1 = df2['Cálculo de similaridade por medida do cosseno']
    y2 = df2['Cálculo de similaridade por path similarity - synsets']
    
    diferenca = y2-y1
    plt.xlabel("Documentos analisados via web scraping")
    #plt.xlabel("Tamanho arquivos analisados em Mb")
    #plt.ylabel("Tempo de processamento em segundos (s)")
    plt.ylabel("Percentual de similaridade calculado (%)")
    
    #plt.title('Análise do tempo de processamento do cálculo da similaridade')
    plt.title('Análise dos valores de similaridade calculados')
    #plt.plot(x, y1, label='Tempo de processamento do cálculo por cosseno')
    plt.plot(x, y1, label='Cálculo de similaridade por medida do cosseno')
    plt.plot(x, y2, label='Cálculo de similaridade por path similarity - synsets')
    #plt.plot(x, diferenca, label='Diferença entre os valores calculados (%)')
    
    #plt.plot(x, y2, label='Tempo de processamento do cálculo por path similarity - synsets')
    plt.legend(loc='upper right')
    plt.show()
    
    
    
    '''
    sns.factorplot("Tempo_analise_similaridade pelo Cosseno", 
                    'Tempo_analise_similaridade por path similarity - synsets',
                    data=df2, 
                    hue="Tamanho arquivos analisados em Mb",
                    height=3,
                    aspect=2)
    
      
    columns=[
         'Url do documento analisado'
         'Tamanho arquivos analisados em Mb',
         'Tempo_analise_similaridade pelo Cosseno',
         'Cálculo de similaridade por medida do cosseno',
         'Tempo_analise_similaridade por path similarity - synsets',
            'Cálculo de similaridade por path similarity - synsets'])
    
    
    '''
    df2.plot(x='Url do documento analisado', y='Tempo_analise_similaridade por path similarity - synsets')
    
    

def treinarClassificador():
    
    caminhoDocs = os.getcwd().replace('src','/documentos_testados/')
    dirs = os.listdir(caminhoDocs)
    #listaArquivosBaixados = [caminhoDocs+file for file in dirs]
    #analise.modelarClassificador(listaArquivosBaixados)
    arquivo1 = 'C:/Users/Dell E5410/Documents/ANALISE_E_DESENVOLVIMENTO_DE_SISTEMAS.pdf'
    arquivo2 = 'C:/Users/Dell E5410/Documents/LAJOP_24_1_1_1_6WQ842B4X2.pdf'#FARMACOBOTÂNICA
    #arquivo2 = 'C:/Users/Dell E5410/Documents/UNINOVE/Artigo.doc(1).docx'#INTELIGENCIA ARTIFICAL E BIOMEDICINA
    nomeArquivo = 'Modelo-de-Artigo-Cientifico.pdf' 
    nomeCaminhoArquivo = os.getcwd().replace('src','/')+'documentos_testados/'+ nomeArquivo
        
    docPositivo = lerDocumento(nomeCaminhoArquivo)
    doc1 = lerDocumento(arquivo1)
    doc2 = lerDocumento(arquivo2)
    
    #doc2 = leituraPDF('C:/Users/Dell E5410/Documents/ANALISE_E_DESENVOLVIMENTO_DE_SISTEMAS.pdf')
    #doc2 = leituraPDF('C:/Users/Dell E5410/Documents/COURSERA/Python - for Informatics.pdf')
    dicionarioPositivo = {}
    dicionarioPositivo[nomeCaminhoArquivo] = docPositivo
    dicionarioNegativo = {}
    dicionarioNegativo[arquivo1] = doc1
    dicionarioNegativo[arquivo2] = doc2
    analise.treinarClassificadorNLTK(dicionarioPositivo, dicionarioNegativo)
    
import bs4 #para documentação da versão
import urllib3 #para documentaão da versão
from PyPDF2 import pdf #para documentaão da versão

global arquivoAnaliseResultadosGerais

def efetuarAnaliseWebScraping():
    
    tempo_antes_analise = time()
            
    nome_arquivo_analise = 'ResultadoAnalise.txt'
    pastaAnalise = os.getcwd().replace('src', 'Analises/'+ str(datetime.now()).replace(':', '-')+'/')
    os.mkdir(pastaAnalise)
    caminho_arquivo_analise = pastaAnalise + nome_arquivo_analise
    file = open(caminho_arquivo_analise, 'w')
    file.write('Análise iniciada: {}'.format(str(datetime.now()).replace(':', '-')))
    #nomeArquivo = 'Plagio_Testes_PDF.pdf'
    #nomeArquivo = 'Haltermanpythonbook.pdf'
    #para testes - disponível em : http://www.jvasconcellos.com.br/unijorge/wp-content/uploads/2013/04/Modelo-de-Artigo-Cientifico.pdf
    
    '''
    print(os.getcwd())
    print(os.listdir())
    print('----------------------------')
    print('Digite o nome e o caminho do arquivo PDF:')
    arq = input()
    if not arq.endswith('.pdf'):
    arq = arq + '.pdf'
    '''
    print('\nPython version ' + sys.version)
    print('\nPandas version ' + pd.__version__)
    print('\nNLTK version ' + nltk.__version__)
    print('\nPyPDF version ' + PyPDF2.__version__)
    print('\nSciKit Learn version ' + sklearn.__version__)
    print('\nMatplotlib version ' + matplotlib.__version__)
    
    file.write('\n****************************************************')
    file.write('\nDOCUMENTAÇÃO DAS VERSÕES DOS PACOTES UTILIZADOS')
    file.write('\nVersão do Python ' + sys.version)
    file.write('\nVersão do Pandas ' + pd.__version__)
    file.write('\nVersão do NLTK   ' + nltk.__version__)
    file.write('\nAutor do NLTK   ' + nltk.__author__)
    file.write('\nVersão do PyPDF  ' + PyPDF2.__version__)
    file.write('\nAutor do PyPDF  ' + pdf.__author__)
    file.write('\nVersão do BeautifulSoup  ' + bs4.__version__)
    file.write('\nAutor do BeautifulSoup  ' + bs4.__author__)
    file.write('\nVersão do Urllib3  ' + urllib3.__version__)
    file.write('\nAutor do UrlLib3  ' + urllib3.__author__)
    file.write('\nVersão do SciKit Learn ' + sklearn.__version__)
    file.write('\nVersão do Matplotlib   ' + matplotlib.__version__)
    file.write('\nVersão do Seaborn   ' + sns.__version__)
    file.write('\n****************************************************')
    
    #temaDocumentoSuspeito = 'modelo_artigo_científico'
    #temaDocumentoSuspeito = 'farmacologia'
    #Normas de Artigo científico
    #nomeArquivo = 'Modelo-de-Artigo-Cientifico.pdf' 
    nomeArquivo = 'ARTIGO_ENIC.txt'
    #nomeCaminhoArquivo = os.getcwd().replace('src','/')+'documentos_testados/'+ nomeArquivo
    #FARMACOLOGIA
    #nomeCaminhoArquivo = 'C:/Users/Dell E5410/Documents/LAJOP_24_1_1_1_6WQ842B4X2.pdf'#FARMACOBOTÂNICA
    #nomeArquivo = 'http___congresso.rebibio.net_congrebio2017_trabalhos_pdf_congrebio2017-et-09-002.pdf'
    #nomeArquivo = 'http___bdm.unb.br_bitstream_10483_1796_1_2011_ZandraZangrandoCardoso.pdf'
    #nomeArquivo = 'http___www.alice.cnptia.embrapa.br_bitstream_doc_748981_1_Kiillcap12.pdf'
    #nomeCaminhoArquivo = os.getcwd().replace('src','/')+'documentos_testados/'+ nomeArquivo
    nomeCaminhoArquivo = 'C:/Users/Dell E5410/Documents/UNINOVE/ENIC_2018/'+nomeArquivo
    
    
    conteudoSuspeito = ''
    if('.txt' in nomeArquivo):
        arquivoSuspeito = open(nomeCaminhoArquivo, encoding='utf-8')
        try:
            conteudoSuspeito = arquivoSuspeito.read()
        finally:
            arquivoSuspeito.close()
    else:
        conteudoSuspeito = lerDocumento(nomeCaminhoArquivo)
    
    '''
    listaPalavras = analise.processaDados(conteudoSuspeito)
    distrFreq = nltk.FreqDist(listaPalavras)
    distrFreq.tabulate()
    print('Máxima frequencia = '+str(max(distrFreq.values())))
    distrFreq.plot(50, cumulative=False)
    frequenciaMaxima = max(distrFreq.values())
    listaPalavras = [palavra for palavra in distrFreq.keys() if distrFreq[palavra]>(frequenciaMaxima*0.9)]
    listaQueries = []
    listaQueries.append(listaPalavras[0] + ' ' + listaPalavras[1] + ' pdf')
    print('Lista de queries: ')
    print(listaQueries)
    listaQueries = listaPalavras
    '''

    listaQueries = list()     
    listaSentencasImportantes = process.processarTexto(conteudoSuspeito)
    #listaQueries = listaSentencasImportantes
    listaQueries = ['"similaridade textual distânicia do cosseno"', 
                    '"similaridade semântica" synsets wordnet',
                    '"similaridade cosseno" "similaridade wordnet" "synsets shortest path distance pdf',
                    'comparação cálculo de similaridade textual cosseno wordnet synsets pdf']
    
    
    #listaPalavrasImportantes = process.processarTexto(conteudoSuspeito)
    #print('Quantidade de sentenças sem stop words a serem pesquisadas no web scraping : {}', len(listaSentencasImportantes))
    #print('Quantidade de palavras sem stop words a serem pesquisadas no web scraping : {}', len(listaPalavrasImportantes))
    #INSTRUÇÃO USADA PARA TESTES
    #listaSentencasSemStopWords = ['Como elaborar um artigo']
    #quantidade_sentencas_scraping = len(listaSentencasImportantes)
    #print('Lista de palavras importantes: ')
    #print(listaPalavrasImportantes)

    #import torrequest
    #file.write(('\nVersão do TorRequest   ' + torrequest.__version__))
    #query = input('Digite o assunto do trabalho analisado :')
    
    #for motor in listaMotoresBusca :
    tempo_antes_scraping = time()
    listaLinks = list()
    listaGlobalLinks = []
    
    for query in listaQueries:    
              
              listaLinks = web.pegaLinksSomenteBeautifulSoup(query, file)
              
              if listaLinks:
                for link in listaLinks :
                #if(link.endswith('.pdf') or link.endswith('.docx') or link.endswith('.doc')):
                    if(str(link).endswith('.pdf')):
                        print(link)
                        listaGlobalLinks.append(link)
                print('Quantidade atual de links na lista global de links: {}'.format(len(listaGlobalLinks)))    
    tempo_scraping = time() - tempo_antes_scraping
    print('Tempo para efetuar web scraping: '+ str(tempo_scraping))
    file.write('\nTempo para efetuar web scraping: '+ str(tempo_scraping))  
    #file.write('\nQuantidade de palavras pesquisadas: '+str(len(listaPalavrasImportantes)))
    #file.write('\nQuantidade de sentenças pesquisadas: '+str(len(listaSentencasImportantes)))
    #print(listaGlobalLinks)
    print('\nQuantidade de links para documentos obtidos via web scraping : {}', len(listaGlobalLinks))
    file.write('\nDOCUMENTO ANALISADO\n')
    file.write(nomeCaminhoArquivo+'\n')
    file.write('\nQuantidade de links para documentos obtidos via web scraping : '+str(len(listaGlobalLinks))+'\n')
    file.write('*************************************************************\n')
    
    for link in listaGlobalLinks:
        file.write(link)
        file.write('\n')
    file.write('*************************************************************\n')
    
    
    conteudoFonte = ''
    df = pd.DataFrame(
         columns=[
         'Url do documento analisado',
         'Tamanho arquivos analisados em Mb',
         'Quantidade de tokens processados,',
         'Tempo_analise_similaridade pelo Cosseno',
         'Cálculo de similaridade por medida do cosseno',
         'Tempo_analise_similaridade por path similarity - synsets',
         'Cálculo de similaridade por path similarity - synsets'])
    i = 1
    nome_arquivo_analise1 = 'ResultadoAnalise_Data_Frame_'+ str(datetime.now()).replace(':', '-')+'.csv'
    path_arquivo_analise = os.getcwd().replace('src', 'Analises/'+nome_arquivo_analise1)
    arquivo_analise1 = open(path_arquivo_analise, 'w')
    arquivo_analise1.write(
        'Url do documento analisado,'+
        'Tamanho arquivos analisados em Mb,'+
        'Quantidade de tokens processados,'+
        'Tempo_analise_similaridade pelo Cosseno,'+
        'Cálculo de similaridade por medida do cosseno,'+
        'Tempo_analise_similaridade por path similarity - synsets,'+
        'Cálculo de similaridade por path similarity - synsets'+'\n')
    
    for url in listaGlobalLinks:
        
        resultadoSynset = 0
        resultadoCosseno = 0
    
        
        if(url.endswith('.pdf')):
            conteudoFonte = extraiConteudoRemoto(url)
        #if(url.endswith('.doc')):
         #   arquivoFonte = doc.leituraDOC(url)
        #Uma questão: quem garante que a fonte obtida também não é plageada
        conteudoDicionario = {}
        conteudoDicionario[nomeCaminhoArquivo] = conteudoSuspeito
        tamanhoArquivoSuspeito = os.path.getsize(nomeCaminhoArquivo)
        file.write('Tamanho do arquivo suspeito {} Mb \n'.format(tamanhoArquivoSuspeito))
        nomeCaminhoArquivoRemoto = os.getcwd().replace('src', 'documentos_testados/')+ url.replace('/', '_').replace(':', '_')
        tamanhoArquivoRemoto = 0
        try:
            tamanhoArquivoRemoto = os.path.getsize(nomeCaminhoArquivoRemoto)
            file.write('Tamanho do arquivo remoto {} Mb \n'.format(tamanhoArquivoRemoto))
        except:
            pass
        tamanhoArquivosAnalisados = tamanhoArquivoSuspeito + tamanhoArquivoRemoto
        
        conteudoDicionario[url] = conteudoFonte
        timeAntesCosseno = time()
        resultadoCosseno = analise.calculaSimilaridadeCosseno(conteudoDicionario)
        timePosCosseno = time()
        tempoAnaliseCosseno = timePosCosseno - timeAntesCosseno
        
        try:
            resultadoCosseno = resultadoCosseno[0][0]
        except:
            print('Problema no cálculo do Cosseno')
            file.write('Problema no cálculo do Cosseno'+'\n')
            pass
        if resultadoCosseno is not None:
            resultadoCosseno = resultadoCosseno * 100
        
        timeAntesSynset = time()
        resultadoSynset = 0
        quantidadeTokens = 0
        try:
            resultadoSynset, quantidadeTokens = analise.testaSimilaridadeSynSets(conteudoSuspeito, conteudoFonte)
        except:                                    
            pass
        
        timePosSynset = time()
        tempoAnaliseSynset = timePosSynset - timeAntesSynset
        
        if quantidadeTokens:
            print('Quantidade de tokes processados: '+str(quantidadeTokens)+'\n')
            file.write('Quantidade de tokes processados: '+str(quantidadeTokens)+'\n')
        
        data = {
                'Url do documento analisado': url, 	
                'Tamanho arquivos analisados em Mb': tamanhoArquivosAnalisados,
                'Quantidade de tokens processados': quantidadeTokens,
                'Tempo_analise_similaridade pelo Cosseno': tempoAnaliseCosseno,
                'Cálculo de similaridade por medida do cosseno': resultadoCosseno,
                'Tempo_analise_similaridade por path similarity - synsets':tempoAnaliseSynset,
                'Cálculo de similaridade por path similarity - synsets':resultadoSynset
                }
        '''
        df2 = pd.DataFrame(data, 
         columns=[
         'Url do documento analisado'
         'Tamanho arquivos analisados em Mb',
         'Quantidade de tokens processados',
         'Tempo_analise_similaridade pelo Cosseno',
         'Cálculo de similaridade por medida do cosseno',
         'Tempo_analise_similaridade por path similarity - synsets',
         'Cálculo de similaridade por path similarity - synsets'],
         index = i)
        ''' 
        #https://www.tutorialspoint.com/python_pandas/python_pandas_dataframe.htm 
        df = df.append(data, ignore_index=True)
        print(df)

        if(resultadoCosseno is not None and resultadoSynset is not None):
            if(resultadoCosseno < 50 and resultadoSynset < 50):
                listaGlobalLinks.remove(url)
                del conteudoDicionario[url]
                arquivoBaixado = os.getcwd().replace('src','/')+'documentos_testados/'+ url.replace('/', '_').replace(':', '_')
                try:
                    listaArquivosBaixados.remove(arquivoBaixado)
                    os.remove(arquivoBaixado)
                except:
                    pass
                #print('SIMILARIDADE CALCULADA POR SYNSET  (%):'+str(resultadoSynset))
                #print('Tempo de Análise por Synset: %0.4fs .'% tempoAnaliseSynset)
                print('SIMILARIDADE CALCULADA POR COSSENO (%) :'+str(resultadoCosseno))
                print('Tempo de Análise por cosseno entre vetores: %0.4fs .'% tempoAnaliseCosseno)
                file.write('*****************************************\n')
                file.write('URL : '+url+'\n')
                file.write('SIMILARIDADE CALCULADA POR SYNSET  (%):'+str(resultadoSynset)+'\n')
                file.write('\nTempo de Análise por Synset em segundos :' + str(tempoAnaliseSynset))
                file.write('\nSIMILARIDADE CALCULADA POR COSSENO (%) :'+str(resultadoCosseno)+'\n')
                file.write('\nTempo de Análise por cosseno da distância entre vetores dos documentos: %0.4fs .'% tempoAnaliseCosseno)
                file.write('*****************************************\n')
            #SE A SIMILARIDADE CALCULADA FOR MAIOR QUE 50%
            else:
                file.write('[LINKS TREINAMENTO]'+url+'[LINKS TREINAMENTO]')
                file.write('*****************************************\n')
                file.write('URL : '+url+'\n')
                file.write('SIMILARIDADE CALCULADA POR SYNSET  (%):'+str(resultadoSynset)+'\n')
                file.write('\nTempo de Análise por Synset em segundos :' + str(tempoAnaliseSynset))
                file.write('\nSIMILARIDADE CALCULADA POR COSSENO (%) :'+str(resultadoCosseno)+'\n')
                file.write('\nTempo de Análise por cosseno da distância entre vetores dos documentos: %0.4fs .'% tempoAnaliseCosseno)
                file.write('\n*****************************************\n')
            #https://pandas.pydata.org/pandas-docs/version/0.21/tutorials.html
        arquivo_analise = 'ResultadoAnalise_Data_Frame_'+ str(datetime.now()).replace(':', '-')+'.csv'
        path_arquivo_analise = os.getcwd().replace('src', 'Analises/'+arquivo_analise)
            #arquivo_csv_analise = open(path_arquivo_analise, 'w')
            #arquivo_csv_analise.close()
    
            
        arquivo_analise1.write(str(url)+','+
                                str(tamanhoArquivosAnalisados)+','+
                                str(quantidadeTokens)+','+
                                str(tempoAnaliseCosseno)+','+
                                str(resultadoCosseno)+','+
                                str(tempoAnaliseSynset)+','+
                                str(resultadoSynset)+'\n')

        
    df.to_csv(path_arquivo_analise, index=False)
    file.write('*********************************************************\n')
    file.write('FIM DA ANÁLISE PARA O DOCUMENTO'+nomeCaminhoArquivo+'\n')
    tempo_apos_analise = time()
    tempo_analise = tempo_apos_analise - tempo_antes_analise
    file.write('Tempo total para análise de semelhança entre documentos: {} s'.format(tempo_analise))
    file.close()
    arquivo_analise1.close()
    #print('Lista de arquivos baixados')
    #print(listaArquivosBaixados)
    #print('Quantidade de arquivos baixados: '+ str(len(listaArquivosBaixados)))
    '''
    #passando flag para não pegar muitos links de exemplos negativos
    lista_href = web.pegaLinksSomenteBeautifulSoup(motor, 'farmacologia pdf', flag='negativos')
    listaLinksNegativos = [link for link in lista_href if str(link).endswith('.pdf')]
    
    dicionarioNegativo = {}
    #PARA TREINAR O CLASSIFICADOR SEM FAZER TODO O PROCESSO
    É possível ler o arquivo ResultadoAnalise e extrair URLS dentro dos delimitadores '[LINKS TREINAMENTO]'
    através da função text.strip('[LINKS TREINAMENTO]')
    
    for url in listaLinksNegativos:
        if(url.endswith('.pdf')):
           conteudoFonte = extraiConteudoRemoto(url)
           dicionarioNegativo[url] = conteudoFonte
    
    analise.treinarClassificadorNLTK(conteudoDicionario, dicionarioNegativo)
    '''

main()