#coding:UTF-8
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
from bs4 import BeautifulSoup
import requests
import urllib.error
import ssl
from urllib.parse import urljoin
from urllib.parse import urlparse
from urllib.request import urlopen
from selenium import webdriver
from selenium.webdriver.common.keys import Keys   
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
#from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from torrequest import TorRequest
import torrequest
# Ignora erros de certificado(SSL)
# Referencia: https://github.com/csev/py4e/blob/master/code3/pagerank/spider.py
'''
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE
'''


#def main():
    #testarTorRequest()

def testarTorRequest():
    #tr=TorRequest(password='JAMAIS COLOCAR SENHA NO CÓDIGO')
    response= requests.get('http://ipecho.net/plain')
    print ("Meu endereço IP original :",response.text)
    
    '''https://stackoverflow.com/questions/29805260/tor-name-not-recognized-in-stem
    Necessário configurar a seguinte instrução:
    tor_cmd ='C:\\Tor Browser\\Browser\\TorBrowser\\Tor\\tor.exe'   
    File "C:\Python\Python36\lib\site-packages\stem\process.py", line 112, in launch_tor
    
    '''
    #tor = TorRequest(password=xxxxxxxxxxx)
    #tor = TorRequest(proxy_port = 9150, ctrl_port=9151, password='xxxxxxxxxxxx')
    tor = TorRequest(password='xxxxxxxxxxxxxxxxxx')
    tor.reset_identity() #Reset Tor
    response= tor.get('http://ipecho.net/plain')
    print ("Endereço Ip enviado na requisição pelo Tor",response.text)
    
    
    
    
    
    
# FUNÇÃO PARA RETORNAR LINKS DADA UMA QUERY PARA PESQUISA E A URL DO MOTOR DE BUSCA
# Retorna uma lista de links

def pegaLinksSomenteBeautifulSoup(query, file, flag='positivos'):


    '''

    #buscaGoogleScholar = "https://scholar.google.com.br/scholar?q=";
    #buscaGoogle = 'https://cse.google.com/cse/publicurl?cx='+search_engine_id+'&key='+google_key
    buscaGoogle = 'https://google.com.br/search'
    buscaGoogleScholar = "https://scholar.google.com.br/search";
    buscaBing = 'https://bing.com/search'

    #PROBLEMAS PARA FAZER SCRAPING COM O GOOGLE
    #listaMotoresBusca = [buscaGoogleScholar, buscaGoogle]
    listaMotoresBusca = [buscaBing]
    #query = query.replace(' ', '+')
    #buscaGoogleScholar = "https://scholar.google.com.br/search?";
    #buscaBing = 'https://bing.com/search?'
    #listaMotoresBusca = [buscaBing]
    #listaMotoresBusca = [buscaGoogleScholar]
    #buscaGoogle = 'https://www.google.com/search?q={}&cx={}&key={}'.format(valorPesquisado, search_engine_id, google_key)

    #headers = {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}
    #headers = {'User-Agent': 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}
    #https://stackoverflow.com/questions/31218983/beautifulsoup-get-cite-tags-from-google
    
    
    
    '''
    
    #busca = {'q':query}
    #valorPesquisado = urllib.parse.urlencode(query)
    print('Query: '+query)
    valorPesquisado = query.replace(' ', '+')
    #print(valorPesquisado)
    #google_key = ""
    #search_engine_id = ""
    '''
    with open('C:/GoogleSearch_credenciais/google_key.txt') as file1:
        for line in file1:
            google_key += line


    with open('C:/GoogleSearch_credenciais/search_engine_id.txt') as file2:
        for line in file2:
            search_engine_id += line
    '''
    lista_motores_busca = []
    buscaGoogleScholar = 'https://scholar.google.com.br/search?q={}'.format(valorPesquisado)
    buscaGoogle = 'https://www.google.com/search?q={}'.format(valorPesquisado)
    buscaBing = 'https://bing.com/search?q={}'.format(valorPesquisado)
    buscaBaidu = 'http://www.baidu.com/s?wd={}&oq={}'.format(valorPesquisado, valorPesquisado)
    buscaQwant = 'https://www.qwant.com/?q={}&t=web'.format(valorPesquisado)
    buscaYahoo = 'https://br.search.yahoo.com/search?p={}'.format(valorPesquisado)
    buscaYandex = 'https://yandex.com/search/?text={}'.format(valorPesquisado)
    #lista_motores_busca = [buscaBing, buscaQwant, buscaBaidu, buscaYahoo, buscaYandex, buscaGoogleScholar, buscaGoogle]
    lista_motores_busca = [buscaBing]
    
    contadorResultados = 0
    #listaLinks = list()
    rangeMax = 20

    nomeArquivo = 'Scraping.txt'
    arquivoSenha = open('C:/Users/Dell E5410/Documents/Scraping/'+nomeArquivo, 'r')
    senha = arquivoSenha.read()
    arquivoSenha.close()
    listaGlobalLinks = list()
    
    for url_motorBusca in lista_motores_busca:
        #Reinicializa o contador para o próximo motor de busca
        contadorResultados = 0
        
        for i in range(0,rangeMax):
        
            url = ""
            if ('google' in url_motorBusca):
                url = url_motorBusca + "&start=" + str(contadorResultados)
            elif('bing' in url_motorBusca):
                url = url_motorBusca + "&first=" + str(contadorResultados)
            elif('baidu' in url_motorBusca):
                url = url_motorBusca + "&pn=" + str(contadorResultados)
            elif('yahoo' in url_motorBusca):
                url = url_motorBusca + "&b=" + str(contadorResultados)
            '''
            NÃO DÁ PARA UTILIZAR O YANDEX NESTE LAÇO POIS ELE APRESENTA POR PÁGINA
            E NÃO POR CONTAGEM DE RESULTADO
            '''
            print('--------------------')
            print(url)
            print('--------------------')
            response = ''
            
            if('google' in url_motorBusca):
                tor = TorRequest(password=senha)
                #tor = TorRequest(proxy_port = 9050, ctrl_port=9151, password=senha)
                response = requests.get('http://ipecho.net/plain')
                print("Meu endereço IP original :", response.text)
                file.write("\nMeu endereço IP original :{}".format(response.text))
                #https://tb-manual.torproject.org/en-US/troubleshooting.html
                #file.write(('\nVersão do TorRequest   ' + torrequest.__version__))
                response= tor.get('http://ipecho.net/plain')
                tor.reset_identity() #Reseta o IP Tor 
                print("Endereço Ip enviado na requisição pelo Tor",response.text)
                file.write("\nEndereço Ip enviado na requisição pelo Tor {} \n Url enviada no request {}".format(response.text, url.encode('utf-8')))
                #response = requests.get(url, headers=obterHeaers())
                response = tor.get(url, headers=obterHeaders())
            
            
            #Se fizer a requisição ao Google sem o Tor 
            #Necessário rotacionar User Agents e proxies
        
            else:
           
                #proxy = pegarProxy()
                #response = requests.get(url,proxies={"http": proxy, "https": proxy}, headers=obterHeaders())
                try:
                    file.write('\nUrl enviada no request: {} '.format(url.encode('utf-8')))
                except:
                    pass
                response = requests.get(url, headers=obterHeaders())
                #response = requests.get(url, headers=obterHeaders())
           
                   #
                    #response.raise_for_status()
            #DOM = urlopen(url, context=ctx)
            #html = DOM.read() 
            #soup = BeautifulSoup(response.text, 'lxml')
            soup = BeautifulSoup(response.text, 'html.parser')
            #print(response.text)
            elementos = []        
            lista_links = list()
        
            '''
            <b>About this page</b><br><br>Our systems have detected unusual traffic from your computer network.  This page checks to see if it&#39;s really you sending the requests, and not a robot.  <a href="#" onclick="document.getElementById('infoDiv').style.display='block';">Why did this happen?</a><br><br>
            <div id="infoDiv" style="display:none; background-color:#eee; padding:10px; margin:0 0 15px 0; line-height:1.4em;">
            This page appears when Google automatically detects requests coming from your computer network which appear to be in violation of the <a href="//www.google.com/policies/terms/">Terms of Service</a>. The block will expire shortly after those requests stop.  In the meantime, solving the above CAPTCHA will let you continue to use our services.<br><br>This traffic may have been sent by malicious software, a browser plug-in, or a script that sends automated requests.  If you share your network connection, ask your administrator for help &mdash; a different computer using the same IP address may be responsible.  <a href="//support.google.com/websearch/answer/86640">Learn more</a><br><br>Sometimes you may be asked to solve the CAPTCHA if you are using advanced terms that robots are known to use, or sending requests very quickly.
            </div>
            
            IP address: 186.204.84.109<br>Time: 2018-08-27T18:44:14Z<br>URL: https://www.google.com/search?q=BIOLOGIA+RE%0APROD%0AUTIVA+DE+D%0AUAS+%0AES%0AP%C3%89%0ACIES+DE+%0AANACARDIACEAE+DA+%0ACAATINGA+AMEA%0A%C3%87ADAS+DE+EXTIN%0A%C3%87%C3%83%0AOL%C3%BAcia+Helena+Piedade+Kiill1,+Carla+Tatiana+de+Vasconcelos+Dias+%0AMartins2+e+Paloma+Pereira+da+Silva2INTRODU%C3%87%C3%83ODas+forma%C3%A7%C3%B5es+vegetais,+considera-se+a+Caatinga+como+um+dos+%0Abiomas+brasileiros+mais+alterados+pelas+atividades+humanas,+n%C3%A3o+%0Ahavendo+at%C3%A9+o+momento+levantamentos+sistem%C3%A1ticos+sobre+a+evolu%C3%A7%C3%A3o+%0A%0Ade+sua+cobertura+vegetal+ao+longo+do+tempo+(Capobianco+2002).&amp;start=70<br>
            </div>
            '''
            '''
            if('google' in url_motorBusca):
            elementos = soup.findAll('cite',attrs={'class':'iUh30'})
            for link in elementos:
                print(link)
                lista_links = [str(link).replace('<strong>','').replace('</strong>','') for link in elementos]
            else:
            '''
        
            elementos = soup('a')
            lista_links = [href.get('href') for href in elementos if str(href.get('href')).endswith('.pdf')]
            
            print('Links obtidos {}'.format(lista_links))
            for link in lista_links:
                print(link)
                listaGlobalLinks.append(str(link))
            #passando flag para não pegar muitos links de exemplos negativos
            if(flag == 'negativos' or ('qwant' in url_motorBusca)):
                return lista_links
            else:
                #incrementa para pegar os próximos 10 resultados    
                contadorResultados += 10
    print('--------------------')
    print('WebScraping - Quantidade parcial de links obtidos nesta iteração via web scraping: '+str(len(lista_links)))    
    print('--------------------')
    return listaGlobalLinks


def pegaLinksComSeleniumBSoup(url_motorBusca, valor_pesquisado):
    
    #Necessário baixar o driver 'geckodriver' e setar o diretorio de instalacao
    # na variável PATH do sistema. Para mim não bastou colocar o caminho
    # do gechodriver no path do sistema. Tentei outras formas conforme abaixo
    # o que funcionou foi colocar o caminho do execut�vel do driver conforme
    # instrução abaixo
    #https://github.com/mozilla/geckodriver/releases
    #https://stackoverflow.com/questions/40208051/selenium-using-python-geckodriver-executable-needs-to-be-in-path
    #https://stackoverflow.com/questions/25713824/setting-path-to-firefox-binary-on-windows-with-selenium-webdriver
    #binaryFirefox = FirefoxBinary('C:/Program Files/Mozilla Firefox/firefox.exe')
    #profile = webdriver.FirefoxProfile()
    #browser = webdriver.Firefox(firefox_binary=binaryFirefox, firefox_profile=profile)
    #browser = webdriver.Firefox()

    #https://stackoverflow.com/questions/46787421/webdriverexception-message-geckodriver-executable-needs-to-be-in-path-for-wi
    path = "C:/SISTEMAS/geckodriver/geckodriver-v0.21.0-win64/geckodriver.exe"
    browser = webdriver.Firefox(executable_path=path)
    
    body = browser.find_element_by_tag_name("body")
    body.send_keys(Keys.CONTROL + 't')
    
    contadorResultados = 0
    listaLinks = []
    
    for i in range(0,20):
        
        elementos = []
        
        #browser.get(url_motorBusca + q + "&start=" + str(contadorResultados))
        browser.get(url_motorBusca)
        campoPesquisa = browser.find_element_by_name('q')
        paginacao = browser.find_element_by_name('start')
        campoPesquisa.clear()
        paginacao.clear()
        campoPesquisa.send_keys(valor_pesquisado)
        paginacao.send_keys(contadorResultados)
        campoPesquisa.submit()
        
        localizacao_resultados = "//div/h3/a"
        WebDriverWait(browser, 10).until( EC.visibility_of_element_located((By.XPATH, resultados)))
        elementos = browser.find_elements(By.XPATH, localizacao_resultados)
        #if('www.google.com' in url_motorBusca):
         #   elementos = browser.find_elements_by_xpath("//h3[@class='r']/a")
        #elif('scholar.google.com.br' in url_motorBusca):
         #   elementos = browser.find_elements_by_xpath("//h3[@class='gs_rt']/a")
        
        for link in elementos:
            listaLinks.append(link)
        
        contadorResultados += 10

    print(listaLinks)
    
    #browser.close()    
        
    return listaLinks    

#https://www.scrapehero.com/how-to-fake-and-rotate-user-agents-using-python-3/
def obterHeaders():
    import random
    listaUserAgents = [
   #Chrome
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
    'Mozilla/5.0 (Windows NT 5.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
    #Firefox
    'Mozilla/4.0 (compatible; MSIE 9.0; Windows NT 6.1)',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)',
    'Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 6.2; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0)',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)',
    'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)',
    'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)']
    url = 'https://httpbin.org/user-agent'
    user_agent = random.choice(listaUserAgents)
    headers = {'User-Agent': user_agent,
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       #'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}
       
    print("User-Agent enviado no request:%s\n"%(user_agent))
    print("-------------------\n\n")
    return headers

import requests
from lxml.html import fromstring
from itertools import cycle
import traceback

#https://www.scrapehero.com/how-to-rotate-proxies-and-ip-addresses-using-python-3/
def pegarProxy():
    
    url = 'https://free-proxy-list.net/'
    response = requests.get(url)
    parser = fromstring(response.text)
    proxies = set()
    for i in parser.xpath('//tbody/tr')[:10]:
        if i.xpath('.//td[7][contains(text(),"yes")]'):
            #Grabbing IP and corresponding PORT
            proxy = ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
            proxies.add(proxy)
    proxy_pool = cycle(proxies)
    proxy = next(proxy_pool)
    return proxy  


#main()
